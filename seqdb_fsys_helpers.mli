(* $Id$ *)

(** Additional functions for filesystems *)

open Netchannels
open Seqdb_fsys_types

class input_channel : 'fd file_system -> 'fd -> in_obj_channel
  (** [let ch = new input_channel fsys file]: Creates an input channel [ch]
      that reads from the file
   *)

class output_channel : 'fd file_system -> 'fd -> out_obj_channel
  (** [let ch = new output_channel fsys file]: Creates an output channel [ch]
      that writes to the file
   *)


val iter_failsafe : 
      ('fd file_system_iteration -> unit) ->
      (exn -> int64 -> bool) ->
      'fd file_system_iterator ->
        unit
  (** Iterates over the files of an fsys in failsafe mode, i.e. unreadable
      parts of the fsys are skipped. Use it as:

      {[ iter_failsafe (fun it -> <e1> ) (fun e pos -> <e2>) iterator ]}

      The code [<e1>] is executed for every readable file. It is ensured
      that [it#current_name], [it#current_pos], and [it#current_file]
      can be called without raising exceptions. The function body [<e2>]
      is called for every unreadable file. [e] is the exception explaining
      the problem. [pos] is the closest file position before the problematic
      area that can be determined. [<e2>] must return [true] if the iteration
      is to be continued, and [false] to stop iterating.
   *)
