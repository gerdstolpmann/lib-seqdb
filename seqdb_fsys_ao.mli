(* $Id$ *)

(** Append-only (ao) filesys variant *)

(** These filesys are restricted in so far:

   - [open_file_wr] is only permitted with new file names so that a new
     file must be created.
   - Furthermore, writing to a file using [write_file] is only permitted
     if the file is still the last one.
   - [file_mtime] reflects the time of creating the file, not the last
     modification.
   - [set_file_mtime] is forbidden
   - [truncate] is forbidden
   - [reserve] is a no-op
   - There are no restrictions for deleting files. Both [delete_file] and
     [delete_name_from_index] work for all files.
   - [rename_file] restricts the length of the new name: it is not allowed
     that the name is so long that a bigger inode must be allocated.
 *)

(** The ao filesystems are special ht filesystems. Because of this, it is
   possible to implement the ao functions by "plugging in" to 
   {!Seqdb_fsys_ht}. Do this by calling the [init] function below.

   So {!Seqdb_fsys_ht.ht_base} is also used to keep handles for the ao variants.
   By calling {!Seqdb_fsys_ht.get_filesys} you simply get the ao variant. All
   the other functions also work, except:

   - {!Seqdb_fsys_ht.create_filesys} cannot create the ao variant. Use
     the [create_ao_filesys] function below instead.
   - {!Seqdb_fsys_ht.get_iterator} works, but iteration starts always at 
     the beginning. Use [get_ao_iterator] below for starting at any
     mtime.
   - {!Seqdb_fsys_ht.get_idx_iterator} works, but iteration starts always at 
     the beginning. There is no ao counterpart with more possibilities.
 *)

open Seqdb_fsys_types
open Seqdb_fsys_ht

type ao_params = 
    { ao_time_mark_period : int;   (** TIMEMP in seconds *)
    }
  (** Additional parameters for the ao variant of file systems *)

exception Append_only_restriction of string
  (** Raised if one of the mentioned restrictions would have been violated.
     The string argument is the path to the filesystem
   *)


val init : unit -> unit
  (** Initialize {!Seqdb_fsys_ht} in a special way so the ao variant is known
     there
   *)

val create_ao_filesys : ht_base -> string -> params -> ao_params ->
                          ht_file_descr file_system
  (** Create an ao file system *)			    

val filesys_ao_params : ht_base -> string -> ao_params
  (** Get the special ao parameters *)

val configure_ao_filesys : ?read_only_mode:bool ->
                           ht_base -> string -> unit
  (** - [read_only_mode]: If true, the fsys can only be read - writes will fail.
   *     The advantage is that the fsys lock can be quicker released.
   *)

val get_ao_iterator : ?at_mtime:int64 ->
                      ht_base -> string -> 
                        ht_file_descr file_system_iterator
  (** Starts iterating at the first file whose [mtime] is greater or equal
     to [at_mtime] (or the beginning of the filesys if [at_mtime] is omitted).
   *)
