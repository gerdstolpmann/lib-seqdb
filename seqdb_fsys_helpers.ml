(* $Id$ *)

open Netchannels
open Seqdb_fsys_types

class raw_input_channel (fsys : 'fd file_system) (fd : 'fd) =
object(self)
  val mutable fpos = 0L

  method input s p l =
    let n = fsys # read_file fd s p fpos l in
    fpos <- Int64.add fpos (Int64.of_int n);
    if n = 0 then raise End_of_file;
    n
    
  method close_in() =
    fsys # close_file fd

  method pos_in =
    Int64.to_int fpos

end


class buf_input_channel fsys fd =
object(self)
  inherit
    buffered_raw_in_channel (new raw_input_channel fsys fd)
  method input_line = 
    self # enhanced_input_line
end


class input_channel fsys fd =
  lift_rec_in_channel (new buf_input_channel fsys fd :> rec_in_channel)


class raw_output_channel (fsys : 'fd file_system) (fd : 'fd) =
object(self)
  val mutable fpos = 0L

  method output s p l =
    fsys # write_file fd s p fpos l;
    fpos <- Int64.add fpos (Int64.of_int l);
    l
    
  method flush() =
    ()

  method close_out() =
    fsys # close_file fd

  method pos_out =
    Int64.to_int fpos
end


class output_channel fsys fd =
object(self)
  inherit buffered_raw_out_channel (new raw_output_channel fsys fd)
  inherit augment_raw_out_channel
end


let iter_failsafe f_success f_error itor =
  let catch_eof f arg =
    try Some(f arg) with End_of_file -> None in

  let rec next_file it =
    let last_pos = it#current_pos in
    try
      it#next();   (* or End_of_file *)
      check_file it it#current_pos
    with
      | End_of_file -> false
      | error ->
	  f_error error last_pos && (
	    match catch_eof it#next_recoverable () with
	      | Some () ->
		  check_file it it#current_pos
	      | None ->
		  false
	  )

  and check_file it pos =
    try
      ignore(it#current_name);
      ignore(it#current_file);
      true
    with
      | End_of_file ->
	  false
      | error ->
	  f_error error pos && (
	    match catch_eof it#next_recoverable () with
	      | Some () ->
		  check_file it it#current_pos
	      | None ->
		  false
	  )
  in

  match catch_eof itor#start () with
    | Some it ->
	if check_file it 0L then (
	  f_success it;
	  while next_file it do
	    f_success it
	  done
	)
    | None ->
	()
