(** sequential log files whose entries are of any type *)

(* Serializer contains a pair of functions to serialize (unserialize) any type to (from) a string *)
module type Serializer =
sig 
  type t 
  val string_of_t : t -> string 
  val t_of_string : string -> t 
end

module Make : functor (B : Serializer) ->
sig
  type t

  val open_log :
    ?create:bool -> 
    ?flush_every:int -> 
    ?auto_sync:int -> 
    string -> 
    t
      (* open a log file.  [flush_every] and [auto_sync] have the
	 meaning identical to that found in module [Kvseq].  When
	 [create] is [true], create the file should it not exist
	 (default [false]) *)

  val close_log : t -> unit
    (* close the log file *)

  val add : t -> B.t -> unit
    (* write an entry to the log file *)

  val num_entries : t -> int64
    (* get the number of entries in the log file *)

  val iter : t -> (Seqdb_containers.Kvseq.pointer -> B.t -> 'a) -> unit
    (* iterate through the entries of a log file *)

end
