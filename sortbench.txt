Benchmark results:

datafile = bucket15.data (2101350400 bytes, 468517 entries)
Test conducted on cl1

Test		chunksize	mem (rss) 	time
----------------------------------------------------------------------
read		100_000_000	350 M		53 s
read		200_000_000	650 M 		55 s
read		400_000_000	1200 M		59 s
read		800_000_000	1900 M		68 s

split		100_000_000	350 M		108 s
split		200_000_000	630 M		99 s
split		400_000_000	1200 M		105 s
split		800_000_000	1900 M		117 s

split -sort	100_000_000	360 M		110 s
split -sort	200_000_000	630 M		105 s
split -sort	400_000_000	1200 M		109 s
split -sort	800_000_000	1900 M		109 s

sort		100_000_000	445 M		404 s
sort 		200_000_000	850 M		444 s
sort		400_000_000	1400 M		316 s
sort		800_000_000	2000 M		248 s
sort		2_100_000_000   2700 M		253 s
