(* $Id$ *)

type level =
    [ `Emerg | `Alert | `Crit | `Err | `Warning | `Notice | `Info ]


let string_of_level =
  function
    | `Emerg -> "emerg"
    | `Alert -> "alert"
    | `Crit -> "crit"
    | `Err -> "err"
    | `Warning -> "warning"
    | `Notice -> "notice"
    | `Info -> "info"


let stderr_logger level msg =
  Printf.eprintf "[%s] %s\n%!"
    (string_of_level level)
    msg


let logger = ref stderr_logger

let log level msg =
  !logger level msg

let logf level fmt = 
  Printf.kprintf (log level) fmt


