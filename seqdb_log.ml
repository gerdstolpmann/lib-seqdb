open Seqdb_containers
open Seqdb_containers.Kvseq

module type Serializer =
sig 
  type t

  val string_of_t : t -> string
  val t_of_string : string -> t

end

module Make ( B : Serializer )  = struct
  type t = {
    kvs : Seqdb_containers.Kvseq.t ;
    fd : Unix.file_descr
  }

  let open_log ?(create=false) ?(flush_every=100) ?(auto_sync=120) file =
    let flags = [ Unix.O_RDWR ] @ (if create then [ Unix.O_CREAT ] else []) in
    let fd = Unix.openfile file flags 0o666 in
    let io = object method file_descr = fd method dispose_hint() = () end in
    let kvs = 
      if create then
	Kvseq.create 
	  ~supports_deletions:false 
	  ~have_statistics:true 
	  ~purpose:"log" 
	  ~keyrepr:(`Fixed 0)  (* don't need keys *)
	  io 
      else
	access io
    in
      configure ~flush_every ~auto_sync:(Some auto_sync) kvs;
      { kvs = kvs ; fd = fd }

  let close_log t =
    sync t.kvs;
    Unix.close t.fd


  let add t q =
    let q_s = B.string_of_t q in
    let c = { delflag = false ; key = "" ; value = q_s } in
    let _e = Kvseq.add t.kvs c in
      ()


  let num_entries t =
    Kvseq.num_entries t.kvs

  let iter t func =
    let rec loop e =
      let c = Kvseq.get_contents e in
      let q = B.t_of_string c.value in
      let p = get_pointer e in
	func p q;
	let e' = next_entry e in
	  loop e'
    in
      try
	loop (first_entry t.kvs) 
      with End_of_file ->
	()


end
