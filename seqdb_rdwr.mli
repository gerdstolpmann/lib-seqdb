(* $Id$ *)

(** Read/write buffer for accessing files *)

(** {2 Descriptors} *)

class type file_descr =
object
  method file_descr : Unix.file_descr
    (** Get the file descriptor or raise an exception if this is not possible.
        This method is called when the descriptor is needed but unknown.
        [file_descr] is only allowed to return a different descriptor
        than before when [dispose_hint] has been called in the meantime.
      *)

  method dispose_hint : unit -> unit
    (** Called by higher layers to notify this object that it is now allowed
        to close the Unix descriptor, and that the next call of [file_descr]
        is permitted to return a different descriptor.
     *)

end
  (** Abstraction of an object providing a file descriptor. The [file_descr]
      method is invoked to get a new descriptor after the old one has been
      disposed.
   *)


class type disposable_descr =
object
  inherit file_descr

  method dispose_descr : unit -> bool
    (** Actually close the descriptor if it is allowed by the protocol.
        Returns [true] whether the descriptor is now closed
     *)
end


class managed_descr : 
        filename:string -> 
        flags:Unix.open_flag list ->
        reopen_flags:Unix.open_flag list ->
        perm:int ->
        reopen_perm:int ->
            disposable_descr
  (** A sample implementation of [disposable_descr]. The file [filename]
      is opened with open [flags] and open [perm]issions. After the first
      successful disposal the file is automatically reopened with
      the [reopen_flags] and the [reopen_perm]issions.
   *)

(** {2 Buffers} *)

class type reader_writer =
object
  method chunk_size : int
    (** The chunk size is the amount of bytes read or written at one time *)

  method pos : int64
    (** Current read/write position *)

  method seek : int64 -> unit
    (** Set the read/write position *)

  method eof : int64
    (** The eof-of-file position *)

  method input : string -> int -> int -> int
    (** [input s pos len]: Read up to [len] bytes at the current read/write
        position into [s] at string position [pos]. Returns the actual number
        of read bytes (which is at most [chunk_size]). If [len>0] the result
        0 means that EOF is reached.
     *)

  method really_input : string -> int -> int -> unit
    (** [really_input s pos len]: Read exactly [len] bytes at the current
        read/write position into [s] at string position [pos].

        If less than [len] bytes are available, [End_of_file] is raised,
        and it is unspecified how many bytes have been put into [s].
     *)

  method output : string -> int -> int -> int
    (** [output s pos len]: Write up to [len] bytes from [s] at the string
        position [pos] to the file at the current read/write position.
        Returns the actual number of read bytes (which is at most
        [chunk_size]). 
     *)

  method really_output : string -> int -> int -> unit
    (** [really_output s pos len]: Write exactly [len] bytes from [s] at
        the string position [pos] to the file at the current read/write
        position.
     *)

  method flush : unit -> unit
    (** Flush all dirty buffers to disk *)

  method dispose_descr : unit -> unit
    (** Forgets the file descriptor. The next access calls the [file_descr]
        method to get a new one.

        Note that this method causes that all cached descriptors are forgotten,
        and the [dispose_hint] method of the descriptor object is called.
        It does not close the descriptor.
     *)
end
  (** A [reader_writer] is a file buffer implementation that supports
      seeking. 
   *)


class buf_rd_wr : ?buffer_size:int -> ?chunk_size:int -> 
                  file_descr -> reader_writer
  (** Buffered [reader_writer] accessing the passed file descriptor.
      The [chunk_size] is the size of the chunks read or written at one
      time (default: 16384). The [buffer_size] is the total amount of
      buffer space which may be higher than [chunk_size]. The buffer
      is managed with a least-recently-used policy. If the [buffer_size]
      is less than [chunk_size], buffering is limited to the file area
      surrounding the current file position (which is the default).
   *)

class sub_rd_wr : ?incr:int64 -> int64 -> reader_writer -> reader_writer
  (** View on the passed [reader_writer]: The size of the file is assumed to
     only be the passed number, or if it is written beyond the old
     assumed size, the written size.

     If [incr] is passed, every time the physical EOF is exceeded,
     [incr] bytes with value 0 are appended.
   *)


(** {2 Utility functions} *)

val input_string : reader_writer -> int -> string
  (** [input_string rw n]: read exactly [n] bytes from [rw] *)

val output_string : reader_writer -> string -> unit
  (** [output_string rw s]: write [s] to [rw] *)

val lockf_tmo : Unix.file_descr -> Unix.lock_command -> int -> float -> bool
  (** [let flag = lockf_tmo fd cmd size tmo]: Like [Unix.lockf], but
      a timer is started, and after [tmo] seconds it is given up to
      get the lock. This is achieved by starting an alarm timer.
      Note that this method is incompatible with multi-threaded programs.

      For the command [F_ULOCK] the call returns always [true].
      For the other commands, the function returns whether the lock could be
      obtained.

      The call may still raise [EDEADLK] and [ENOLCK], but not
      [EAGAIN] or [EINTR].

      A zero [tmo] turns [F_LOCK] into [F_TLOCK] and [F_RLOCK] into
      [F_TRLOCK]. A negative [tmo] disables the timeout entirely.
   *)
