(* $Id$ *)

type file_name = string
type file_type = char

exception File_not_found of file_name
exception File_type_mismatch of file_name
exception File_exists of file_name

type wr_flags =
    [ `Excl
    | `Mtime of int64
    ]

class type ['file_descr] file_system =
object
  method exclusive_access : unit -> unit
  method open_file_rd : file_name -> file_type list -> 'file_descr
  method open_file_wr : file_name -> file_type list -> file_type option -> 
                          ('file_descr * bool)
  method open_file_wr_ext : wr_flags list ->
                            file_name -> file_type list -> file_type option -> 
                              ('file_descr * bool)
  method file_size : 'file_descr -> int64
  method file_type : 'file_descr -> file_type
  method file_mtime : 'file_descr -> int64
  method set_file_type : 'file_descr -> file_type -> unit
  method set_file_mtime : 'file_descr -> int64 -> unit
  method read_file : 'file_descr -> string -> int -> int64 -> int -> int
  method write_file : 'file_descr -> string -> int -> int64 -> int -> unit
  method truncate : 'file_descr -> int64 -> unit
  method reserve : 'file_descr -> int64 -> unit
  method close_file : 'file_descr -> unit
  method delete_file : 'file_descr -> unit
  method delete_name : file_name -> unit
  method delete_name_from_index : file_name -> unit
  method rename_file : 'file_descr -> file_name -> unit
  method filepos : 'file_descr -> int64
  method is_last_file : 'file_descr -> bool
  method cmp_filepos : file_name -> file_name -> int
  method guess_filepos : file_name -> int64 option
  method checkpoint : ?soft:bool -> unit -> unit
  method dispose : unit -> unit
  method superblock_variable : string -> int64
  method set_superblock_variable : string -> int64 -> unit
end


class type ['file_descr] file_system_iterator =
object
  method start : unit -> 'file_descr file_system_iteration
end


and ['file_descr] file_system_iteration =
object
  method current_name : file_name
  method current_file : 'file_descr
  method current_pos : int64
  method next : unit -> unit
  method next_recoverable : unit -> unit
end
