(* $Id$ *)

(** Common definitions for the whole [seqdb] library *)

type level =
    [ `Emerg | `Alert | `Crit | `Err | `Warning | `Notice | `Info ]
   (** Log level. Does intentionally not contain [`Debug] *)

val logger : (level -> string -> unit) ref
  (** the current logging function. By default, it is logged to stderr
      without printing timestamps
   *)

val log : level -> string -> unit
  (** log a string *)

val logf : level -> ('a, unit, string, unit) format4 -> 'a
  (** log some number of values with a format string *)
